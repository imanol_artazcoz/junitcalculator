

public class Calculator {
	
	public int sum(int n1, int n2){
		return n1+n2;
	}
	/***
	 * Función encargada de realizar la suma de dos parámetros de entrada
	 * @param n1
	 * @param n2
	 * @return inter que muestra la suma
	 */
	public int substract(int n1, int n2) {
		return n1-n2;
	}
	/***
	 * Función encargada de realizar la resta de dos parámetros de entrada
	 * @param n1
	 * @param n2
	 * @return inter que muestra la resta
	 */
	public int division(int n1, int n2) {
		return n1/n2;
	}
	/***
	 * Función encargada de realizar la división de dos parámetros de entrada
	 * @param n1
	 * @param n2
	 * @return inter que muestra la división
	 */
	public int multiplicacion(int n1, int n2) {
		return n1*n2;
	}
	/***
	 * Función encargada de realizar la multiplicación de dos parámetros de entrada
	 * @param n1
	 * @param n2
	 * @return inter que muestra la multiplicación
	 */
}
