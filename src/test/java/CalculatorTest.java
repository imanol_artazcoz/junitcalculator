import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
	private Calculator target;
	@Before
	public void setUp() throws Exception {
		target = new Calculator();
	}

	@Test
	public void testSub() {
		int expected = 22;
		int actual = target.substract(30,8);
		assertEquals("Testing sub" ,expected, actual);
	}
	
	@Test
	public void testMult() {
		int expected = 10;
		int actual = target.multiplicacion(2,5);
		assertEquals("Testing sub" ,expected, actual);
	}
	
	@Test
	public void testDiv() {
		int expected = 20;
		int actual = target.division(40,2);
		assertEquals("Testing sub" ,expected, actual);
	}
	
}
